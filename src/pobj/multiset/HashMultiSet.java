package pobj.multiset;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Un multi-ensemble contenant des éléments de classe T
 * 
 * @author BARROCA Quentin et Paul Genries
 * @param <T>
 */
public class HashMultiSet<T> extends AbstractCollection<T> implements MultiSet<T> {
	/**
	 * multEns le multi-ensemble version hashmap 
	 */
	private HashMap<T, Integer> multEns;

	/**
	 * Le nombre d'elements du multi-ensemble
	 */
	private int size;

	public HashMap<T, Integer> getMultEns() {
		return multEns;
	}

	/**
	 * Constructeur de multi-ensemble en hashmap
	 */
	public HashMultiSet() {
		multEns = new HashMap<T, Integer>();
	}

	/**
	 * Constructeur de multi-ensemble à l'aide de c
	 * 
	 * @param c une collection
	 */
	public HashMultiSet(Collection<T> c) {
		multEns = new HashMap<T, Integer>();
		size = 0;
		for (T elem : c) {
			this.add(elem);
		}
	}

	@Override

	/**
	 * ajouter un element count fois
	 * 
	 * @param elem  l'element
	 * @param count le nombre d'elements à ajouter
	 */
	public boolean add(T elem, int count) {
		if (multEns.containsKey(elem)) {
			int val = multEns.get(elem).intValue();
			multEns.put(elem, val + count);
		} else {
			multEns.put(elem, count);
		}
		// System.out.println(multEns.toString());
		size += count;
		return true;
	}

	@Override
	/**
	 * ajouter un element
	 * 
	 * @param elem l'element
	 */
	public boolean add(T elem) {
		// TODO Auto-generated method stub
		return add(elem, 1);
	}

	@Override
	/**
	 * Enleve un element
	 * 
	 * @param e
	 */
	public boolean remove(Object e) {
		// TODO Auto-generated method stub
		return remove(e, 1);
	}

	@Override

	/**
	 * Enleve un element e count fois
	 */
	public boolean remove(Object e, int count) {
		// TODO Auto-generated method stub
		T elem = (T) e;

		if (!multEns.containsKey(elem)) {
			return false;
		}

		int value = multEns.get(elem).intValue();

		if (value - count < 0) {
			return false;
		} else {
			multEns.remove(elem);
		}
		return true;
	}

	@Override
	/**
	 * @return le nombre de fois où est present cet element 
	 * @param o l'element
	 */
	public int count(T o) {
		// TODO Auto-generated method stub
		return multEns.get(o).intValue();
	}

	@Override

	public void clear() {
		// TODO Auto-generated method stub
		multEns.clear();
		size = 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Iterator<T> iterator() {
		return new HashMultiSetIterator();
	}

	/**
	 * 
	 * iterateur qui parcourt d'abord un element element count fois puis passe a
	 * l'element suivant
	 *
	 */
	private class HashMultiSetIterator implements Iterator<T>, Map.Entry<T, Integer> {
		private T key;
		private int poskey;
		Iterator<Map.Entry<T, Integer>> iterateur;

		public HashMultiSetIterator() {
			iterateur = multEns.entrySet().iterator();
			key = null;
			poskey = 1;
		}

		@Override
		public boolean hasNext() {
			return (iterateur.hasNext() || poskey < multEns.get(key));
		}

		@Override
		public T next() {
			if (key == null) {
				if (size == 0)
					return null;
				key = iterateur.next().getKey();
				poskey = 1;
				return key;
			} else {
				// Si on a plusieurs fois cet element on retourne son nombre d'iterations
				if (poskey < multEns.get(key)) { /** retourne la liste des elements sans doublons */

					poskey++;
					return key;
					// sinon on change d'element
				} else {
					key = iterateur.next().getKey();
					poskey = 1;
					return key;
				}
			}

		}

		@Override
		public T getKey() {
			return key;
		}

		@Override
		public Integer getValue() {
			return new Integer(poskey);
		}

		@Override
		public Integer setValue(Integer value) {
			poskey = value.intValue();
			return null;
		}

	}

	@Override
	/**
	 * retourne la liste des elements sans doublons
	 */
	public List<T> elements() {
		// TODO Auto-generated method stub
		List<T> res = new ArrayList<T>();
		for (Map.Entry<T, Integer> map : multEns.entrySet()) {
			res.add(map.getKey());
		}
		return res;

	}

}
