package pobj.multiset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pobj.util.Chrono;

/**
 * 
 * @author Barroca quentin et Genries Paul
 *
 */
public class WordCount {

	/**
	 * Le multiSet qui sera casté soit en HashMultiSet soit NaiveMultiSet
	 */
	private static MultiSet<String> ms1;

	/**
	 * affiche les 1O premiers mots presents dans un texte 
	 * @param ms le multi-ensemble
	 */
	public static void wordcount(MultiSet<String> ms) {
		String file = "data/WarAndPeace.txt";
		if (ms instanceof HashMultiSet) {
			ms1 = (HashMultiSet) ms;
		}
		if (ms instanceof NaiveMultiSet) {
			ms1 = (NaiveMultiSet) ms;

		}
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				for (String word : line.split("\\P{L}+")) {
					if (word.equals(""))
						continue; // ignore les mots vides 6
					ms.add(word);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<String> ms_sans_db = ms.elements();

		Collections.sort(ms_sans_db, new ElementsSortingComparator());

		for (int i = 0; i < 10; i++) {
			if (ms_sans_db.size() <= i)
				break;
			System.out.println(ms_sans_db.get(i));

		}

	}

	/**
	 * 
	 * Comparateur qui compare les elements des multi-ensembles
	 *
	 */
	private static class ElementsSortingComparator implements Comparator<String> {

		@Override
		public int compare(String e1, String e2) {
			// for comparison
			if (ms1.count(e1) > ms1.count(e2)) {
				return -1;
			}
			if (ms1.count(e1) < ms1.count(e2)) {
				return 1;
			}

			return 0;
		}
	}

	public static void main(String[] args) {
		HashMultiSet<String> m = new HashMultiSet<String>();
		NaiveMultiSet<String> nms = new NaiveMultiSet<String>();
		Chrono chrono = new Chrono();
		wordcount(m);
		chrono.stop();
		Chrono chrono2 = new Chrono();
		wordcount(nms);
		chrono2.stop();
	}
}
