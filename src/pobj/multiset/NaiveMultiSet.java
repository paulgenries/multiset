package pobj.multiset;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Barroca Quentin Genries Paul
 *
 * @param <T>
 */
public class NaiveMultiSet<T> extends AbstractCollection<T> implements MultiSet<T> {
	
	/** multEns version arrayList*/
	private ArrayList<T> multEns;

	private int size;
	
	public NaiveMultiSet() {
		multEns=new ArrayList<T>();
	}
	

	public NaiveMultiSet(Collection<T> c) {
		multEns=new ArrayList<T>();
		size=0;
		for(T elem: c) {
			this.add(elem);
		}
	}
	@Override
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return multEns.iterator();
	}
	

	@Override
	public boolean add(T e, int count) {
		// TODO Auto-generated method stub
		for(int i=0;i<count;i++) {
			multEns.add(e);
		}
		size+=count;
		return true;
	}

	@Override
	public boolean add(T e) {
		// TODO Auto-generated method stub
		multEns.add(e);
		size++;
		return true;
	}

	@Override
	public boolean remove(Object e) {
		// TODO Auto-generated method stub
		return remove(e,1);
	}

	@Override
	public boolean remove(Object e, int count) {
		T elem=(T) e; 
		
		if(!multEns.contains(elem)) {
			return false;
		}
	
		if(count(elem)<count) {
			return false;
		}
		for(int i=0;i<count;i++) {
			multEns.remove(elem);
		}
		return true;
	}

	@Override
	/**
	 * @param element o
	 * @return le nombre de fois où l'element o est present
	 */
	public int count(T o) {
		int total=0;
		T elem = (T) o;
		for(T e : multEns) {
			if(e.equals(elem)) { //TODO a verifier 
				total++;
			}
		}
		return total;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		multEns.clear();
		size=0;
	}

	@Override
	
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	/**
	 * @return la liste des elements sans doublons
	 */
	public List<T> elements() {
		// TODO Auto-generated method stub
		List<T> res=new ArrayList<T>();
		for(T e : multEns) {
			if(!res.contains(e)) res.add(e);
		}
		return res;
	}


}
